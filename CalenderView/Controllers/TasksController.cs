﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CalenderView.Models;

namespace CalenderView.Controllers
{
    public class TasksController : Controller
    {
        private EventsEntities db = new EventsEntities();

        // GET: Tasks
        public ActionResult Index(int? month, int? year)
        {
            int monthv = month ?? DateTime.Today.Month;
            int yearv = year ?? DateTime.Today.Year;
            // kaks parameetrit, puudumisel võtame jooskva aasta ja/või jooksva kuu

            DateTime startdate = new DateTime(yearv, monthv, 1); // kuu algus
            DateTime calstart = startdate.AddDays(-(((int)startdate.DayOfWeek+6)%7));
            // mis kuupäevast peaks algama kalender (mul kulus hea 15 min selle valmi jaoks)

            // kõik 4 paneme viewsse kaasa
            ViewBag.StartDate = startdate;
            ViewBag.CalStart = calstart; 
            ViewBag.Month = monthv;
            ViewBag.Year = yearv;

            // Nüüd teeme kuus nädalat kuupäevi
            ViewBag.Dates = Enumerable
                .Range(0, 42)               // kuus nädalat (42)
                .GroupBy(x => x / 7)        // grupeerime 7 kaupa
                .Select(x => x.Select(y => calstart.AddDays(y)).ToList())
                        // iga grupi teisendame kuupäevade listiks
                .ToList()       // ja kõik need grupid paneme omakorda listi
                ;
            // tulemuseks on umbes Datetime[6][7] massiiv, aga tegelikult List<List<DateTime>>

            // aga viewle mudeliks anname oma taskid - läheme nüüd seda viewd vaatama
            return View(db.Tasks
//                .AsEnumerable()
//                .Where(x => x.StartDate?.Year <= yearv && x.EndDate?.Year >= yearv)
                .ToList());
        }

        // GET: Tasks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StartDate,EndDate,Name,Description")] Task task)
        {
            if (ModelState.IsValid)
            {
                db.Tasks.Add(task);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(task);
        }

        // GET: Tasks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // POST: Tasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StartDate,EndDate,Name,Description")] Task task)
        {
            if (ModelState.IsValid)
            {
                db.Entry(task).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(task);
        }

        // GET: Tasks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Task task = db.Tasks.Find(id);
            db.Tasks.Remove(task);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
